# Dienststellen


Die Dienststellen des THW werden aus OpenStreetMap mittels Overpass täglich geladen und aufbereitet sowie im Format geojson abgespeichert.

Änderungen übertrage ich manuell in eine Kopie um zu verhindern, dass Fehler in OpenStreetMap dazu führen, dass der Datensatz beschädigt wird.

Darstellung auf einer Karte: [https://umap.openstreetmap.de/de/map/dienststellen-thw_26198](https://umap.openstreetmap.de/de/map/dienststellen-thw_26198)

.gitlab-ci.yml inspiriert durch: [https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen](https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen)


Quelle Taktische Zeichen: [https://github.com/jonas-koeritz/Taktische-Zeichen](https://github.com/jonas-koeritz/Taktische-Zeichen)
