import overpass
import geojson
import geopandas as gpd
from shapely.geometry import Point
import argparse

# Abfrage ueber overpass
api = overpass.API(timeout=500)
result = api.get("""area["name"="Deutschland"]->.boundaryarea;
                 (
                 node["thw:lv"](area.boundaryarea);
                 way["thw:lv"](area.boundaryarea);
                 relation["thw:lv"](area.boundaryarea);
                 node["thw:ltg"](area.boundaryarea);
                 way["thw:ltg"](area.boundaryarea);
                 relation["thw:ltg"](area.boundaryarea)
                 ;)"""
                 ,responseformat="geojson", verbosity="geom")

filename1 = 'osmExport.geojson'
filename2 = 'osm_center.geojson'

# Abfrage speichern
with open(filename1, 'w') as f:
   geojson.dump(result, f)
# Abfrage als GeoDataFrame einlesen
df = gpd.read_file(filename1)
# Mittelpunkt berechnen
df['centerPoint'] = df['geometry'].centroid
df['geometry'] = df['centerPoint']
df.drop(columns = 'centerPoint',inplace=True)

# alle Dienststellen in OSM sollten eine Geometrie und einen Namen haben
df2 = df[["geometry","name"]]
# Liste mit weiteren keys, die die Dienststellen in OSM haben koennen
liste = ["ref:thw",'addr:street','addr:housenumber','addr:city','addr:postcode','addr:place','contact:phone',
         'contact:fax','contact:email','contact:website','phone','fax','email','website','thw:lv','thw:rb',
         'contact:city','contact:housenumber','contact:postcode','contact:street']

#keys nur hinzufuegen, wenn vorhanden (relevant nur fuer wenige)
for i in liste:
    if i in df.columns:
        df2 = df2.join(df[i])

df = df2.copy(deep=False); del df2
# Koordinaten extrahieren in lat und lon
df["lon"]=df.geometry.apply(lambda p: p.x)
df["lat"]=df.geometry.apply(lambda p: p.y)
df.drop(columns = ['geometry'],inplace=True)
# auf sinnvolle Anzahl an Dezimalstellen gerundete Koordinaten
df["lon"] = df["lon"].round(6)
df["lat"] = df["lat"].round(6)

# wieder einen brauchbagen GeoDataFrame erzeugen
geometry = [Point(xy) for xy in zip(df.lon, df.lat)]
df = gpd.GeoDataFrame(df, crs="EPSG:4326", geometry=geometry)

# Link zu Google Maps basteln
df["GMaps"] = "https://www.google.com/maps/dir/?api=1&destination=" + df["lat"].astype(str) + "," + df["lon"].astype(str)
df.drop(columns = ['lat'],inplace=True)
df.drop(columns = ['lon'],inplace=True)

# ggf. tags von altem Kontaschema aufs neue Kontaktschma verschieben
for i in range(len(df)):
    if 'website' in df.columns and df['website'][i] is not None and df['contact:website'][i] is None:
        df.loc[i,'contact:website'] = df['website'][i]    
    if 'phone' in df.columns and df['phone'][i] is not None and df['contact:phone'][i] is None:
        df.loc[i,'contact:phone'] = df['phone'][i]
    if 'fax' in df.columns and df['fax'][i] is not None and df['contact:fax'][i] is None:
        df.loc[i,'contact:fax'] = df['fax'][i]
    if 'email' in df.columns and df['email'][i] is not None and df['contact:email'][i] is None:
        df.loc[i,'contact:email'] = df['email'][i]
    if 'contact:city' in df.columns and df['contact:city'][i] is not None:
        df.loc[i,'addr:city'] = df['contact:city'][i]
    if 'contact:housenumber' in df.columns and df['contact:housenumber'][i] is not None:
        df.loc[i,'addr:housenumber'] = df['contact:housenumber'][i]
    if 'contact:postcode' in df.columns and df['contact:postcode'][i] is not None:
        df.loc[i,'addr:postcode'] = df['contact:postcode'][i]
    if 'contact:street' in df.columns and df['contact:street'][i] is not None:
        df.loc[i,'addr:street'] = df['contact:street'][i]

try:        
    df.drop(columns = ['website'],inplace=True)
    df.drop(columns = ['phone'],inplace=True)
    df.drop(columns = ['fax'],inplace=True)
    df.drop(columns = ['email'],inplace=True)
    df.drop(columns = ['contact:city'],inplace=True)
    df.drop(columns = ['contact:housenumber'],inplace=True)
    df.drop(columns = ['contact:postcode'],inplace=True)
    df.drop(columns = ['contact:street'],inplace=True)
except Exception:
    pass

# Sortierung nach Namen, kleine Anpassungen
df.sort_values(by=['name'],inplace=True, ascending=True)
df['name'] = df['name'].str.replace("THW ","")
df['thw:lv'] = df['thw:lv'].str.replace("THW ","")
df['thw:rb'] = df['thw:rb'].str.replace("THW ","")
df['name'] = df['name'].str.replace(" Technisches Hilfswerk","")
df.loc[:,'contact:website'] = df.loc[:,'contact:website'].str.rstrip("/")
df.loc[:,'contact:website'] = df.loc[:,'contact:website']+"/"

# Schluessel basteln fuer Darstellungszwecke
df["type"] = ""
df["type"][df['name'].str.contains("Landesverband")] = "LV"
df["type"][df['name'].str.contains("Ortsverband")] = "OV"
df["type"][df['name'].str.contains("Leitung")] = "Ltg"
df["type"][df['name'].str.contains("Regionalstelle")] = "Rst"
df["type"][df['name'].str.contains("ogistik")] = "Log"
df["type"][df['name'].str.contains("Ausbildung")] = "AZ"
df["type"][df['name'].str.contains("Stützpunkt")] = "Stp"
df["type"][df['name'].str.contains("Info")] = "Info"

df.fillna("",inplace=True)

argp = argparse.ArgumentParser()
argp.add_argument("--output", required=True)
args = argp.parse_args()

df.to_file(args.output, driver='GeoJSON')
